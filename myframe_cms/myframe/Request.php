<?php
/*
 * @Author: your name
 * @Date: 2021-06-29 15:36:22
 * @LastEditTime: 2021-06-29 16:01:29
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \myframe_cms\myframe\Request.php
 */
namespace myframe;

class Request
{
    protected $pathinfo;
    protected $action;

    public function pathinfo()
    {
        if (is_null($this->pathinfo)) {
            $this->pathinfo = $this->server('PATH_INFO')
            ? $this->server('PATH_INFO')
            : $this->server('REDIRECT_PATH_INFO');
        }
        return ltrim($this->pathinfo, '/');
    }

    public function setAction($action)
    {
        $this->action = $action;
    }

    public function action()
    {
        return $this->action ?: '';
    }
    
    //  server get post isAjax
    public function server($name, $default = null)
    {
        return isset($_SERVER[$name]) ? $_SERVER[$name] : $default;
    }

    public function get($name, $default = null)
    {
        return isset($_GET[$name]) ? $_GET[$name] : $default;
    }

    public function post($name, $default = null)
    {
        return isset($_POST[$name]) ? $_POST[$name] : $default;
    }

    public function isAjax()
    {
        return $this->server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest';
    }

    public function hasFile($name)
    {
        return isset($_FILES[$name]['error']) && $_FILES[$name]['error'] !== UPLOAD_ERR_NO_FILE;
    }

    public function file($name)
    {
        $file = isset($_FILES[$name]) ? $_FILES[$name] : [];
        return Upload::create($file);
    }
}
