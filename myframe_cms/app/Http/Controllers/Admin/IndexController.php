<?php
/*
 * @Author: your name
 * @Date: 2021-05-20 11:36:46
 * @LastEditTime: 2021-05-25 09:33:26
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \myframe\app\Http\Controllers\Admin\IndexController.php
 */
namespace App\Http\Controllers\Admin;

use myframe\DB;

class IndexController extends CommonController
{
    public function index()
    {
        // 获取系统信息
        $this->assign('server_info', [
            'server_version' => $this->request->server('SERVER_SOFTWARE'),
            'mysql_version' => $this->getMySQLVer(),
            'upload_max_filesize' => ini_get('file_uploads') ? ini_get('upload_max_filesize') : '已禁用',
            'max_execution_time' => ini_get('max_execution_time') . '秒',
            'server_time' => date('Y-m-d H:i:s', time())
        ]);
        return $this->fetch('admin/index');
    }
    protected function getMySQLVer()
    {
        $db = DB::getInstance();
        $res = $db->fetchRow('SELECT VERSION() AS ver');
        return $res ? $res['ver'] : '未知';
    }
}
