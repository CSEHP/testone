<?php
/*
 * @Author: your name
 * @Date: 2021-05-20 11:36:43
 * @LastEditTime: 2021-05-25 09:38:25
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \myframe\app\Http\Controllers\Admin\CommonController.php
 */
namespace App\Http\Controllers\Admin;

use myframe\Controller;

class CommonController extends Controller
{
    protected $checkLoginExclude = [];
    protected function initialize()
    {
        session_start();
        if (!isset($_SESSION['cms'])) {
            $_SESSION = ['cms' => []];
        }
        $action = $this->request->action();
        if (in_array($action, $this->checkLoginExclude)) {
            return;
        }
        if (empty($_SESSION['cms']['admin'])) {
            $this->redirect('/admin/login/index');
        } else {
            $user = $_SESSION['cms']['admin'];
            $this->assign('user', $user);
        }
        if (!$this->request->isAjax()) {
            $this->display('admin/layout');
        }
    }
}
