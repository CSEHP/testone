<?php
namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use myframe\Page;
use Exception;
use HTMLPurifier;

class ArticleController extends CommonController
{
    public function index(Article $article)
    {
        //TODO:实现分页查询文章的功能

        //获取当前页码
        $page = $this->request->get('page', 1);
        //定义每页显示两条数据
        $size = 2;
        // 计算偏移量
        $offset = ($page - 1) * $size;
        // 获取当前页数据
        $data = $article->orderBy('id', 'DESC')->limit($offset, $size)->get(['id', 'title', 'author', 'show', 'views', 'created_at']);
        // 给视图模板中的变量赋值
        $this->assign('article', $data);
        // 获取数据总条数
        $total = $article->count();
        // 获取页码器字符串
        $this->assign('page_html', Page::html('?page=', $total, $page, $size));
        // 返回视图文件
        return $this->fetch('admin/article_list');
    }


    public function edit(Article $article, Category $category)
    {
        //TODO:展示文章编辑页面

        // 获取文章id
        $id = $this->request->get('id');

        // 初始化文章数据 （若id存在就返回根据id查询到的数据 不然就返回空数据）
        if ($id) {
            $data = $article->where('id', $id)->first();
        } else {
            $data = ['cid' => 0, 'title' => '', 'author' => '', 'show' => 1,
            'content' => '', 'image' => ''];
        }
        // 给视图模板中的变量赋值
        $this->assign('data', $data);
        // 获取栏目列表
        $data = $category->orderBy('sort', 'ASC')->get();
        //给视图模板中的变量赋值
        $this->assign('category', $data);
        $this->assign('id', $id);
        // 返回视图文件
        return $this->fetch('admin/article_edit');
    }

    public function save(Article $article, HTMLPurifier $purifier)
    {
        //TODO:保存文章信息
        // 获取文章id
        $id = $this->request->post('id', 0);
        // 获取表单数据
        $data = [
            'cid' => $this->request->post('cid', 0),
            'title' => $this->request->post('title', ''),
            'author' => $this->request->post('author', ''),
            'show' => $this->request->post('show', '0'),
            'content' => $this->request->post('content', ''),
        ];
        // 处理上传文件
        $data['content'] = $purifier->purify($data['content']);
        if ($this->request->hasFile('image')) {
            $data['image'] = $this->uploadImage();
        }
        // 如果id存在代表是文章之前存在 则执行修改操作 否则 就是新增操作 就保存用户提交上的文章信息
        if ($id) {
            $article->where('id', $id)->update($data);
            $this->success('修改完成。');
        } else {
            $data['created_at'] = date('Y-m-d H:i:s');
            $article->insert($data);
            $this->success('添加完成。');
        }
    }

    protected function uploadImage()
    {
        try {
            $file = $this->request->file('image');
            $allow_ext = ['jpg', 'jpeg', 'png', 'gif', 'bmp'];
            $ext = $file->extension();
            if (!in_array(strtolower($ext), $allow_ext)) {
                $this->error('文件上传失败：只允许扩展名：' . implode(', ', $allow_ext));
            }
            $sub = date('Y-m/d');
            $name = $file->move('./uploads/images/' . $sub);
            return $sub . '/' . $name;
        } catch (Exception $e) {
            $this->error('文件上传失败：' . $e->getMessage());
        }
    }


    public function delete(Article $article)
    {
        $id = $this->request->get('id');
        $article->where('id', $id)->delete($id);
        $this->success('删除完成。');
    }
}
