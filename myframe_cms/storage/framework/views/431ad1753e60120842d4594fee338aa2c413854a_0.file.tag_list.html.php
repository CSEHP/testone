<?php
/* Smarty version 3.1.34-dev-7, created on 2021-06-24 10:59:57
  from 'F:\myframe_cms\myframe\myframe\resources\views\admin\tag_list.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_60d3f52dcdde22_13057096',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '431ad1753e60120842d4594fee338aa2c413854a' => 
    array (
      0 => 'F:\\myframe_cms\\myframe\\myframe\\resources\\views\\admin\\tag_list.html',
      1 => 1624503596,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60d3f52dcdde22_13057096 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="main-title">
  <h2>标签管理</h2>
</div>
<div class="main-section form-inline">
  <a href="/admin/tag/edit" class="btn btn-success">+ 新增</a>
</div>
<div class="main-section">
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>标签名</th>
        <th>排序值</th>
        <th>显示状态</th>
        <th>操作</th>
      </tr>
    </thead>
  <tbody>
      <?php if ($_smarty_tpl->tpl_vars['tag']->value) {?>
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['tag']->value, 'v');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
?>
      <tr>
        <td><a href="/admin/tag/edit?id=<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['name'];?>
</a></td>
        <td><?php echo $_smarty_tpl->tpl_vars['v']->value['sort'];?>
</td>
        <td><?php if ($_smarty_tpl->tpl_vars['v']->value['status']) {?>显示<?php } else { ?>隐藏<?php }?></td>
        <td>
          <a href="/admin/tag/edit?id=<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" style="margin-right:5px;">编辑</a>
          <a href="/admin/tag/delete?id=<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" class="j-del text-danger">删除</a>
        </td>
      </tr>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
      <?php } else { ?>
      <tr>
        <td colspan="6" class="text-center">列表为空</td>
      </tr>
      <?php }?>
    </tbody>
  </table>
  <div class="text-center"><?php echo $_smarty_tpl->tpl_vars['page_html']->value;?>
</div>
</div>
<?php echo '<script'; ?>
>
  main.menuActive('tag');
  $('.j-del').click(function() {
    if (confirm('您确定要删除此项？')) {
      main.ajaxPost($(this).attr('href'), function() {
        main.contentRefresh();
      });
    }
    return false;
  });
<?php echo '</script'; ?>
><?php }
}
