<?php
/* Smarty version 3.1.34-dev-7, created on 2021-06-24 11:02:41
  from 'F:\myframe_cms\myframe\myframe\resources\views\admin\tag_edit.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_60d3f5d1000ea8_36227791',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9257e0ad95e9198ab0e922712931cefad88c1612' => 
    array (
      0 => 'F:\\myframe_cms\\myframe\\myframe\\resources\\views\\admin\\tag_edit.html',
      1 => 1624503610,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60d3f5d1000ea8_36227791 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="main-title">
  <h2><?php if ($_smarty_tpl->tpl_vars['id']->value) {?>修改<?php } else { ?>添加<?php }?>标签</h2>
</div>
<div class="main-section">
  <form method="post" action="/admin/tag/save" class="j-form">
    <ul class="form-group form-inline">
      <li>
        <input type="text" class="form-control" name="name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['name'];?>
" required>
        <label>标签名</label>
      </li>
      <li>
        <input type="number" class="form-control" name="sort" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['sort'];?>
" required>
        <label>排序值</label>
      </li>
      <li>
        <label>
          <input type="checkbox" name="status" value="1" <?php if ($_smarty_tpl->tpl_vars['data']->value['status']) {?>checked <?php }?> > 隐藏
        </label>
      </li>
      <li>
        <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">
        <input type="submit" value="提交表单" class="btn btn-primary">
        <a href="/admin/tag/index" class="btn btn-default">返回列表</a>
      </li>
    </ul>
  </form>
</div>
<?php echo '<script'; ?>
>
  main.menuActive('tag');
  main.ajaxForm('.j-form', function () {
    main.content('/admin/tag/index');
  });
<?php echo '</script'; ?>
><?php }
}
