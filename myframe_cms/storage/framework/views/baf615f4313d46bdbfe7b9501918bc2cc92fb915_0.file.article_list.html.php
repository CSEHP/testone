<?php
/* Smarty version 3.1.34-dev-7, created on 2021-06-03 11:11:54
  from 'F:\myframe_cms\myframe\myframe\resources\views\admin\article_list.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_60b8487a26b8e6_46646281',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'baf615f4313d46bdbfe7b9501918bc2cc92fb915' => 
    array (
      0 => 'F:\\myframe_cms\\myframe\\myframe\\resources\\views\\admin\\article_list.html',
      1 => 1622689710,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60b8487a26b8e6_46646281 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="main-title">
  <h2>文章管理</h2>
</div>
<div class="main-section form-inline">
  <a href="/admin/article/edit" class="btn btn-success">+ 新增</a>
</div>
<div class="main-section">
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>标题</th>
        <th>作者</th>
        <th>已发布</th>
        <th>阅读量</th>
        <th>创建时间</th>
        <th>操作</th>
      </tr>
    </thead>
    <tbody>
      <?php if ($_smarty_tpl->tpl_vars['article']->value) {?>
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['article']->value, 'v');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
?>
      <tr>
        <td>
          <a href="/admin/article/edit?id=<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>
</a>
        </td>
        <td><?php echo $_smarty_tpl->tpl_vars['v']->value['author'];?>
</td>
        <td><?php if ($_smarty_tpl->tpl_vars['v']->value['show']) {?>是<?php } else { ?>否<?php }?></td>
        <td><?php echo $_smarty_tpl->tpl_vars['v']->value['views'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['v']->value['created_at'];?>
</td>
        <td>
          <a href="/admin/article/edit?id=<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" style="margin-right:5px;">编辑</a>
          <a href="/admin/article/delete?id=<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" class="j-del text-danger">删除</a>
        </td>
      </tr>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
      <?php } else { ?>
      <tr>
        <td colspan="6" class="text-center">列表为空</td>
      </tr>
      <?php }?>
    </tbody>
  </table>
  <div class="text-center"><?php echo $_smarty_tpl->tpl_vars['page_html']->value;?>
</div>
</div>
<?php echo '<script'; ?>
>
  main.menuActive('article');
  $('.j-del').click(function() {
    if (confirm('您确定要删除此项？')) {
      main.ajaxPost($(this).attr('href'), function() {
        main.contentRefresh();
      });
    }
    return false;
  });
<?php echo '</script'; ?>
><?php }
}
