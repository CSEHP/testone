<?php
/*
 * @Author: your name
 * @Date: 2021-05-16 10:27:06
 * @LastEditTime: 2021-06-29 16:02:22
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /myframe/config/database.php
 */
return [
    'type' => 'mysql',          // 数据库类型
    'host' => '127.0.0.1',      // 主机名
    'port' => '3306',           // 端口号
    'dbname' => 'myframe',      // 数据库名
    'charset' => 'utf8',        // 字符集
    'user' => 'root',           // 用户名
    'pwd' => 'WAN123',          // 密码
    'prefix' => 'cms_'          // 表前缀
];
